/*
* isJSON function tests if a String is a valid JSON string. (returns boolean)
* */
function isJSON(JSONObject){
    if(typeof JSONObject !== "string"){
        return false;
    }
    try{
        JSON.parse(JSONObject);
        return true;
    } catch(error){
        return false;
    }
}

jQuery(document).ready(function(){

    window.addEventListener("message", function(evt){

        if(isJSON(evt.data)){
            if(typeof JSON.parse(evt.data).track !== "undefined"){

                _trackgate.push(JSON.parse(evt.data));

            }
        }

    }, false);

});